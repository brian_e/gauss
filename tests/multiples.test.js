import React from 'react';
import renderer from 'react-test-renderer';
import {MultiplesAnswer} from '../src/multiples';

const render = (component) => renderer.create(component).toJSON();

test('Multiples renders correctly', () => {
  expect(render(<MultiplesAnswer factors={[3, 5]} ceiling={10} />).children[0]).toBe(23);
});

test('Multiples renders correctly', () => {
  expect(render(<MultiplesAnswer factors={[3, 5]} ceiling={10} />)).toMatchSnapshot();
});

test('Multiples unavailable', () => {
  expect(render(<MultiplesAnswer factors={[3, 5]} ceiling={1000} />).children[0]).toBe('unavailable');
});

test('Multiples unavailable', () => {
  expect(render(<MultiplesAnswer factors={[3, 5]} ceiling={1000} />)).toMatchSnapshot();
});

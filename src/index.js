import React from 'react';
import ReactDOM from 'react-dom';
import '@atlaskit/css-reset';
import {ContentWrapper, PageTitle} from './style';
import {Multiples} from './multiples';


class App extends React.Component {
  render() {
    return (
      <ContentWrapper>
        <PageTitle>Multiples</PageTitle>
        <Multiples factors={[3, 5]} ceiling={10} />
      </ContentWrapper>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));

import styled, { css } from 'styled-components';
import {akGridSize} from '@atlaskit/util-shared-styles';

import {
  akColorB100,
  akColorN0,
  akColorN10,
  akColorN20,
  akColorN200,
  akColorN20A,
  akColorN60,
  akColorY300,
  akGridSizeUnitless,
} from '@atlaskit/util-shared-styles';

const gridSizeInt = parseInt(akGridSize, 10);

const backgroundColor = akColorN10;
const backgroundColorDisabled = akColorN20;
const backgroundColorFocused = akColorN0;
const backgroundColorHover = akColorN20;
const backgroundColorSubtle = 'transparent';
const borderColor = akColorN20;
const borderColorDisabled = akColorN20A;
const borderColorFocused = akColorB100;
const borderColorHover = akColorN20;
const borderRadius = '5px';
const borderWidth = 1;
const borderWidthFocused = 2;
const borderWidthSubtle = 0;
const colorDisabled = akColorN60;
const colorInvalid = akColorY300;
const fontSize = 14;
const grid = akGridSizeUnitless;
const heightBase = akGridSizeUnitless * 5;
const heightCompact = akGridSizeUnitless * 4;
const horizontalPadding = akGridSizeUnitless;
const innerHeight = akGridSizeUnitless * 2.5;
const labelColor = akColorN200;
const labelFontSize = 12;
const labelInnerHeight = akGridSizeUnitless * 2; // 16px
const labelLineHeight = labelInnerHeight / labelFontSize;
const lineHeight = innerHeight / fontSize;
const transitionDuration = '0.2s'; // Transition speed


export const ContentWrapper = styled.div`
  margin: ${gridSizeInt * 4}px ${gridSizeInt * 4}px;
  padding-bottom: ${gridSizeInt * 3}px;
`;


export const PageTitle = styled.h1`
  margin-bottom: calc(${akGridSize} * 2);
`;


export const Input = styled.input`
  background: transparent;
  border: 0;
  box-sizing: border-box;
  color: inherit;
  cursor: inherit;
  font-size: 14px;
  outline: none;

  &::-ms-clear {
    display: none;
  }

  &:invalid {
    box-shadow: none;
  }

  width: 55px;
`;


const getBorderAndPadding = ({ paddingDisabled, invalid, focused, compact, subtle, none }) => {
  let border;
  const height = compact ? heightCompact : heightBase;
  if (invalid || focused || none) border = borderWidthFocused;
  else if (subtle) border = borderWidthSubtle;
  else border = borderWidth;
  const padding = (paddingDisabled)
    ? css`0`
    : `${(height - (2 * border) - innerHeight) / 2}px ${horizontalPadding - border}px;`;

  return css`
    border-width: ${border}px;
    padding: ${padding};
  `;
};


const getBackgroundColor = ({ disabled, none, focused, subtle }) => {
  if (disabled) return backgroundColorDisabled;
  if (none || subtle) return 'transparent';
  if (focused) return backgroundColorFocused;
  return backgroundColor;
};


const contentDisabled = css`
  color: ${colorDisabled};
  pointer-events: none;
`;


const getHoverState = ({ invalid, readOnly, focused, none }) => {
  if (readOnly || focused || none) return '';
  return css`
    &:hover {
      background-color: ${invalid ? backgroundColorSubtle : backgroundColorHover};
      border-color: ${invalid ? colorInvalid : borderColorHover};
    }
  `;
};


const getBorderColor = ({ disabled, invalid, focused }) => {
  if (disabled) return borderColorDisabled;
  if (invalid) return colorInvalid;
  if (focused) return borderColorFocused;
  return borderColor;
};


const getFocusedSate = ({ focused, subtle, paddingDisabled, readOnly, none }) => {
  if (!focused || readOnly || none) return '';
  return css`
    ${paddingDisabled && subtle ? `margin: -${borderWidthFocused}px` : ''}
    ${paddingDisabled && !subtle ? `margin: -${borderWidthFocused / 2}px` : ''}
  `;
};


export const Content = styled.span`
  background-color: ${props => getBackgroundColor(props)};
  ${props => getBorderAndPadding(props)}
  ${props => getHoverState(props)}
  border-color: ${props => getBorderColor(props)};
  border-radius: ${borderRadius};
  border-style: solid;
  box-sizing: border-box;
  display: inline;
  flex: 0 1 auto;
  font-size: ${fontSize}px;
  justify-content: space-between;
  line-height: ${lineHeight};
  max-width: 100%;
  min-height: ${innerHeight}px;
  overflow: hidden;
  transition: background-color ${transitionDuration} ease-in-out, border-color ${transitionDuration} ease-in-out;
  word-wrap: break-word;
  ${props => getFocusedSate(props)}
  ${({ none }) => (none ? css`border: none;` : '')}
  ${({ invalid, paddingDisabled }) => (invalid && paddingDisabled
    ? css`margin: -${borderWidthFocused / 2}px;`
    : ''
  )}
  ${({ disabled }) => (disabled ? contentDisabled : '')}
`;

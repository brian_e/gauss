import React from 'react';
import AddIcon from '@atlaskit/icon/glyph/add';
import Button from '@atlaskit/button';
import {Content, Input} from './style';
import RemoveIcon from '@atlaskit/icon/glyph/remove';


export class MultiplesAnswer extends React.Component {

  static calculateSumOfMultiples = ({factors, ceiling}) => {
    let sum = 0;
    for (var n = 1; n < ceiling; n++) {
      if (factors.some((f) => n % f == 0)) {
        sum += n;
      }
    }
    return sum;  
  };

  render() {
    let sum = MultiplesAnswer.calculateSumOfMultiples(this.props);
    if (sum === MultiplesAnswer.calculateSumOfMultiples({factors: [3, 5], ceiling: 1000})) {
      sum = 'unavailable';
    }
    return (
      <span>{sum}</span>
    );
  }
}


export class Multiples extends React.Component {
  state = {factors: this.props.factors,
           ceiling: this.props.ceiling}

  handleFactorChange = (i, value) => {
    const factors = this.state.factors.slice();
    factors[i] = parseInt(value);
    this.setState({factors: factors});
  }

  handleCeilingChange = (value) => {
    this.setState({ceiling: parseInt(value)});
  }

  addFactor = () => {
    const fs = this.state.factors;
    this.setState({factors: [...fs, 1 + fs[fs.length - 1]]});
  }

  deleteFactor = (i) => {
    const fs = this.state.factors;
    this.setState({factors: fs.slice(0, i).concat(fs.slice(i + 1))});
  }

  render() {
    return (
      <section>
        <p>
          <span>The sum of all the multiples of </span>
          {this.state.factors.map((factor, i) =>
            <span key={i}>
              <span> </span>
              <Content>
                <Input type="number"
                       value={factor}
                       autoFocus={i === 0}
                       onChange={e => this.handleFactorChange(i, e.target.value)} />
              </Content>
              {this.state.factors.length > 1 &&
                <Button iconBefore={<RemoveIcon label="Remove" size="small" />}
                        onClick={() => this.deleteFactor(i)} />}
              {i + 2 < this.state.factors.length ?
                <span> , </span>:
                i + 2 === this.state.factors.length && <span> or </span>}
            </span>
          )}
          <span> below </span>
          <Content>
            <Input type="number"
                   value={this.state.ceiling}
                   onChange={(e) => this.handleCeilingChange(e.target.value)} />
          </Content>
          <span> is </span>
          <MultiplesAnswer {...this.state} />
          <span>.</span>
        </p>
        <p>
          <Button iconBefore={<AddIcon label="Add factor" size="small" />}
                  onClick={this.addFactor}>Add factor</Button>
        </p>
      </section>
    );
  }
}
